import {Component, HostListener, Input, OnInit} from '@angular/core';
import {mangaService} from "../service/manga.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AngularFirestore, AngularFirestoreDocument} from "@angular/fire/compat/firestore";
import {Manga} from "../model/manga";
import {Chapter} from "../model/chapter";

@Component({
  selector: 'app-manga-chapter',
  templateUrl: './manga-chapter.component.html',
  styleUrls: ['./manga-chapter.component.scss']
})
export class MangaChapterComponent implements OnInit {
  private baseUrl = 'http://localhost:8081/api/';
  private fireStoreChapterImages: any;

  constructor(private mangaService: mangaService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private fire: AngularFirestore) {


  }
  id:any;
  chapterInfo: any;
  pageInfoPrev: any;
  pageInfoNext: any;
  mangaInfo: any;
  mangalist: any;
  mangaId: any;
  getmangaName: any;
  chapterId:any;
  chapterPage: any;
  changeChapter: any;


  ngOnInit(): void {
    this.getDataFromFirestore()
  }

  getDataFromFirestore(): void {
    let url = this.router.url;
    let getMangaName = url.split('/')[2]
    let chapterId = url.split('/')[4]

    this.fire.collection('getManga').doc(getMangaName).valueChanges().subscribe(  s=> {
      this.mangaInfo = s;

      for(let i = 0; i<this.mangaInfo?.chapters.length; i++){
        if(this.mangaInfo.chapters[i].id == chapterId){
          this.chapterInfo = this.mangaInfo.chapters[i];
        }
      }
    },error => {
      console.log("Error for loading data");
    },() => {
    })



  }





  nextChapter(){
    let url = this.router.url;
    let chapterId = url.split('/')[4]

    for(let i = 0; i<this.mangaInfo?.chapters.length; i++){
      if(this.mangaInfo.chapters[i].id == chapterId){
        if(i-1 != -1){
          this.chapterInfo = this.mangaInfo.chapters[i-1];
        }

        let newChapterId = this.chapterInfo.id
        let urlChapterName = this.chapterInfo.chapterUrl.split('/')[5];
        let getMangaName =  this.chapterInfo.chapterUrl.split('/')[4];
        let mangaName = this.chapterInfo.chapterUrl;

        this.getChapterImages(newChapterId,mangaName)
        this.router.navigate(['manga/' + getMangaName + '/' +urlChapterName + '/' + newChapterId]);
      }
    }
  }




  prevChapter(){
    let url = this.router.url;
    let chapterId = url.split('/')[4]
    let iNum;
    for(let i = 0; i<this.mangaInfo?.chapters.length; i++){
      if(this.mangaInfo.chapters[i].id == chapterId){
        if(i+1 != this.mangaInfo.chapters.length){
          this.chapterInfo = this.mangaInfo.chapters[i+1];

        }

        let newChapterId = this.chapterInfo.id
        let urlChapterName = this.chapterInfo.chapterUrl.split('/')[5];
        let mangaName = this.chapterInfo.chapterUrl;
        let getMangaName =  this.chapterInfo.chapterUrl.split('/')[4];
        this.getChapterImages(newChapterId,mangaName)

        this.router.navigate(['manga/' + getMangaName + '/' +urlChapterName + '/' + newChapterId]);
      }
    }
  }

  getChapterImages(chapterID: any, mangaUrl: any) {
    let mangaName = mangaUrl.split('/')[4];
    let chapter = mangaUrl.split('/')[5]
    this.fire.doc("getManga/" + mangaName).valueChanges().subscribe(s => {
      this.chapterInfo = s;
      for (let i = 0; i < this.chapterInfo.chapters.length; i++) {
        if (this.chapterInfo?.chapters[i].id == chapterID) {
          if (this.chapterInfo?.chapters[i].chapterImages == null) {
            this.mangaService.createChapterImages(mangaName, chapter).subscribe((result: any) => {
              this.fireStoreChapterImages = result;
              console.log(result)
              this.saveChapterImagesToFirestore(mangaName, this.fireStoreChapterImages, chapterID);
            }, error => {
              console.log("There is an error!!")
            }, () => {
              console.log("Completed")
            });
          }else{
            this.ngOnInit()
          }
          break;
        }
      }
    }, error => {
      console.log("Error for loading data");
    }, () => {
      console.log("Completed")
    })
  }

  saveChapterImagesToFirestore(mangaName: any, chapterImages: any, chapterID: any) {
    let ref: AngularFirestoreDocument<Manga> = this.fire.doc('getManga/' + mangaName);
    for (let i = 0; i < this.chapterInfo?.chapters.length; i++) {
      if (this.chapterInfo.chapters[i].id == chapterID) {
        if (this.chapterInfo.chapters[i].chapterImages == null) {
          this.chapterInfo.chapters[i].chapterReaded = true;
          this.chapterInfo.chapters[i].chapterImages = chapterImages;
          this.setRandomIdChapterImages();
        }
        break;
      }
    }
    ref.set(this.chapterInfo, {
      merge: true
    }).then(r => {
      console.log("show nothing");
    })
    this.ngOnInit()
  }

  setRandomIdChapterImages() {
    this.chapterInfo.chapters.forEach((res: any) => {
      if (res.chapterImages != null) {
        res.chapterImages.forEach((resultImage: any) => {
          resultImage.id = this.fire.createId();
        })
      }
    })
  }
}
