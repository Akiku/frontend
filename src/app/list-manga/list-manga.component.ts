import {Component, OnInit} from '@angular/core';
import {mangaService} from "../service/manga.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AngularFirestore, AngularFirestoreDocument} from "@angular/fire/compat/firestore";
import {Manga} from "../model/manga";

@Component({
  selector: 'app-list-manga',
  templateUrl: './list-manga.component.html',
  styleUrls: ['./list-manga.component.scss']
})
export class ListMangaComponent implements OnInit {

  constructor(private mangaService: mangaService,
              private fire: AngularFirestore,
              private route: ActivatedRoute,
              private router: Router) {
  }

  mangaList: any[] = [];
  mangaId: any;
  chapterInfo: any;
  id: any
  getMangaName: any;
  mangaListLoaded: boolean = true;
  fireStoreChapterImages: any;


  ngOnInit(): void {
    this.getDataFromFirestore();
  }

  getDataFromFirestore(): void {
    this.fire.collectionGroup('getManga').valueChanges().subscribe(s => {
      this.mangaList = s;
      this.mangaListLoaded = false;
    }, error => {
      console.log("Error for loading data");
    }, () => {
      this.mangaListLoaded = false;
    })
  }

  getChapterImages(chapterID: any, mangaUrl: any) {
    let mangaName = mangaUrl.split('/')[4];
    let chapter = mangaUrl.split('/')[5]
    this.fire.doc("getManga/" + mangaName).valueChanges().subscribe(s => {
      this.chapterInfo = s;
      for (let i = 0; i < this.chapterInfo.chapters.length; i++) {
        if (this.chapterInfo.chapters[i].id == chapterID) {
          if (this.chapterInfo.chapters[i].chapterImages == null) {
            this.mangaService.createChapterImages(mangaName, chapter).subscribe((result: any) => {
              this.fireStoreChapterImages = result;
              this.saveChapterImagesToFirestore(mangaName, this.fireStoreChapterImages, chapterID);
            })
          }else if(this.chapterInfo.chapters[i].chapterImages != null && this.chapterInfo.chapters[i].chapterReaded == false){
            this.saveChapterReadedToFirestore(mangaName,chapterID)
            break;
          }
          break;
        }
      }
    }, error => {
      console.log("Error for loading data");
    }, () => {
    })
  }

  saveChapterReadedToFirestore(mangaName: any, chapterID: any){
    let ref: AngularFirestoreDocument<Manga> = this.fire.doc('getManga/' + mangaName);
    for (let i = 0; i < this.chapterInfo.chapters.length; i++) {
      if (this.chapterInfo.chapters[i].id == chapterID) {
        if (this.chapterInfo.chapters[i].chapterImages != null) {
          this.chapterInfo.chapters[i].chapterReaded = true;
        }
        break;
      }
    }

    ref.set(this.chapterInfo, {
      merge: true
    }).then(r => {
      console.log("show nothing");
    })
  }

  saveChapterImagesToFirestore(mangaName: any, chapterImages: any, chapterID: any) {
    let ref: AngularFirestoreDocument<Manga> = this.fire.doc('getManga/' + mangaName);
    for (let i = 0; i < this.chapterInfo.chapters.length; i++) {
      if (this.chapterInfo.chapters[i].id == chapterID) {
        if (this.chapterInfo.chapters[i].chapterImages == null) {
          this.chapterInfo.chapters[i].chapterReaded = true;
          this.chapterInfo.chapters[i].chapterImages = chapterImages;
          this.setRandomIdChapterImages();
        }
        break;
      }
    }

    ref.set(this.chapterInfo, {
      merge: true
    }).then(r => {
      console.log("show nothing");
    })
  }

  setRandomIdChapterImages() {
    this.chapterInfo.chapters.forEach((res: any) => {
      if (res.chapterImages != null) {
        res.chapterImages.forEach((resultImage: any) => {
          resultImage.id = this.fire.createId();
        })
      }
    })
  }

  //Go To Chapters
  getChapterInfo(chapterUrl: any, chapterId: any) {
    let url = chapterUrl;
    this.getMangaName = url.split('/')[4];
    let chapterName = url.split('/')[5];
    this.router.navigate(['manga/' + this.getMangaName + '/' + chapterName + '/' + chapterId]);
  }

  goToMangaInfo(urlId: any) {
    let url = this.mangaList[urlId].mangaUrl;
    this.getMangaName = url.split('/')[4];
    this.router.navigate(["info-manga/" + this.getMangaName + "/" + this.mangaList[urlId].id])
  }

  delete(docName: any) {
    let getName = docName.split('/')[4];
    console.log(getName)
    if (window.confirm('Are sure you want to delete this item ?')) {
      this.fire.collection('getManga').doc(getName).delete().then(r => {
        console.log("Manga Deleted " + "- " + docName + " -")
      })
      this.ngOnInit();
    }
  }


}
