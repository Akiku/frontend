import {Chapter} from "./chapter";

export interface Manga {

  uid: string,
  mangaTitle: string,
  mangaUrl: string,
  status: string,
  updated: string,
  description: string,
  // chapters: Chapter[],
}
