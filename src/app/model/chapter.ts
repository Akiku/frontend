import {ChapterImage} from "./chapter-image";

export interface Chapter{
  uid: string,
  chapterTitle: string,
  chapterUrl: string,
  chapterReaded: boolean,
  chapterImages: ChapterImage[],
}
