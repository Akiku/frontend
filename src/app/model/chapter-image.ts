export interface ChapterImage {
  uid: string,
  src: string,
}
