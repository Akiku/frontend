import {Component, OnInit} from '@angular/core';
import {mangaService} from "../service/manga.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AngularFirestore, AngularFirestoreDocument} from "@angular/fire/compat/firestore";
import {Manga} from "../model/manga";




@Component({
  selector: 'app-info-manga',
  templateUrl: './info-manga.component.html',
  styleUrls: ['./info-manga.component.scss']
})
export class InfoMangaComponent implements OnInit {

  constructor(private mangaService: mangaService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private fire: AngularFirestore) {
  }

  mangaInfo: any;
  id:any;
  showMore = false;
  getmangaName:any;
  chapterInfo: any
  fireStoreChapterImages: any

  ngOnInit(): void {
    this.getMangaInfo()
  }


  getMangaInfo(){
    let url = this.router.url;
    let getMangaName = url.split('/')[2]

    this.fire.collection('getManga').doc(getMangaName).valueChanges().subscribe(  s=> {
      this.mangaInfo = s;
    },error => {
      console.log("Error for loading data");
    },() => {
    })
  }



  tabLoadTimes: Date[] = [];

  getTimeLoaded(index: number) {
    if (!this.tabLoadTimes[index]) {
      this.tabLoadTimes[index] = new Date();
    }
    return this.tabLoadTimes[index];
  }

  goHome(){
    this.router.navigate(['/list-manga']);
  }

  // 'manga/:nameManga/chapter/:id/:idManga'
  getChapterInfo(chapterUrl: any,chapterId:any) {
    let url =  chapterUrl;
    let getMangaName = url.split('/')[4];
    let chapterName = url.split('/')[5];
    this.router.navigate(['manga/' + getMangaName + '/' +chapterName + '/' + chapterId]);
  }

  getChapterImages(chapterID: any, mangaUrl: any) {
    let mangaName = mangaUrl.split('/')[4];
    let chapter = mangaUrl.split('/')[5]
    this.fire.doc("getManga/" + mangaName).valueChanges().subscribe(s => {
      this.chapterInfo = s;
      for (let i = 0; i < this.chapterInfo.chapters.length; i++) {
        if (this.chapterInfo.chapters[i].id == chapterID) {
          if (this.chapterInfo.chapters[i].chapterImages == null) {
            this.mangaService.createChapterImages(mangaName, chapter).subscribe((result: any) => {
              this.fireStoreChapterImages = result;
              this.saveChapterImagesToFirestore(mangaName, this.fireStoreChapterImages, chapterID);
            }, error => {
              console.log("There is an error!!")
            }, () => {
              console.log("Completed")
            });
          }else if(this.chapterInfo.chapters[i].chapterImages != null && this.chapterInfo.chapters[i].chapterReaded == false){
            this.saveChapterReadedToFirestore(mangaName,chapterID)
            break;
          }
          break;
        }
      }
    }, error => {
      console.log("Error for loading data");
    }, () => {
    })
  }

  saveChapterReadedToFirestore(mangaName: any, chapterID: any){
    let ref: AngularFirestoreDocument<Manga> = this.fire.doc('getManga/' + mangaName);
    for (let i = 0; i < this.chapterInfo.chapters.length; i++) {
      if (this.chapterInfo.chapters[i].id == chapterID) {
        if (this.chapterInfo.chapters[i].chapterImages != null) {
          this.chapterInfo.chapters[i].chapterReaded = true;
        }
        break;
      }
    }

    ref.set(this.chapterInfo, {
      merge: true
    }).then(r => {
      console.log("show nothing");
    })
  }

  saveChapterImagesToFirestore(mangaName: any, chapterImages: any, chapterID: any) {
    let ref: AngularFirestoreDocument<Manga> = this.fire.doc('getManga/' + mangaName);
    for (let i = 0; i < this.chapterInfo.chapters.length; i++) {
      if (this.chapterInfo.chapters[i].id == chapterID) {
        if (this.chapterInfo.chapters[i].chapterImages == null) {
          this.chapterInfo.chapters[i].chapterReaded = true;
          this.chapterInfo.chapters[i].chapterImages = chapterImages;
          this.setRandomIdChapterImages();
        }
        break;
      }
    }

    ref.set(this.chapterInfo, {
      merge: true
    }).then(r => {
      console.log("show nothing");
    })
  }

  setRandomIdChapterImages() {
    this.chapterInfo.chapters.forEach((res: any) => {
      if (res.chapterImages != null) {
        res.chapterImages.forEach((resultImage: any) => {
          resultImage.id = this.fire.createId();
        })
      }
    })
  }



}
