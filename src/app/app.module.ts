import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {FormsModule} from "@angular/forms";
import {ListMangaComponent} from './list-manga/list-manga.component';
import {MatTreeModule} from "@angular/material/tree";
import {NavbarComponent} from './navbar/navbar.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatListModule} from "@angular/material/list";
import {InfoMangaComponent} from './info-manga/info-manga.component';
import {MangaChapterComponent} from './manga-chapter/manga-chapter.component';
import {MatTabsModule} from "@angular/material/tabs";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatTableModule} from "@angular/material/table";
import {MatSidenavModule} from "@angular/material/sidenav";
import {CreateMangaComponent} from './create-manga/create-manga.component';
import {ScrollingModule} from "@angular/cdk/scrolling";
import {LoadingSpinnerComponent} from './components/structure/loading-spinner/loading-spinner.component';
import {AngularFireModule} from "@angular/fire/compat";
import {AngularFirestoreModule} from "@angular/fire/compat/firestore";
import {environment} from "../environments/environment";
import {AngularFireAuthModule} from "@angular/fire/compat/auth";
import { DashboardComponent } from './account/dashboard/dashboard.component';
import { SignInComponent } from './account/sign-in/sign-in.component';
import { ForgotPasswordComponent } from './account/forgot-password/forgot-password.component';
import { SignUpComponent } from './account/sign-up/sign-up.component';
import { VerifyEmailComponent } from './account/verify-email/verify-email.component';
import {AuthService} from "./shared/services/auth.service";

@NgModule({
  declarations: [
    AppComponent,
    ListMangaComponent,
    NavbarComponent,
    InfoMangaComponent,
    MangaChapterComponent,
    CreateMangaComponent,
    LoadingSpinnerComponent,
    DashboardComponent,
    SignInComponent,
    ForgotPasswordComponent,
    SignUpComponent,
    VerifyEmailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    FormsModule,
    MatTreeModule,
    MatToolbarModule,
    MatListModule,
    MatTabsModule,
    MatPaginatorModule,
    MatTableModule,
    MatSidenavModule,
    ScrollingModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
