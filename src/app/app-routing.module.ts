import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AppComponent} from "./app.component";
import {InfoMangaComponent} from "./info-manga/info-manga.component";
import {ListMangaComponent} from "./list-manga/list-manga.component";
import {MangaChapterComponent} from "./manga-chapter/manga-chapter.component";
import {CreateMangaComponent} from "./create-manga/create-manga.component";
import {SignInComponent} from "./account/sign-in/sign-in.component";
import {SignUpComponent} from "./account/sign-up/sign-up.component";
import {DashboardComponent} from "./account/dashboard/dashboard.component";
import {ForgotPasswordComponent} from "./account/forgot-password/forgot-password.component";
import {VerifyEmailComponent} from "./account/verify-email/verify-email.component";

const routes: Routes = [
  {path: '', component: ListMangaComponent},
  {path: 'list-manga', component: ListMangaComponent},
  {path: 'info-manga/:nameManga/:id', component: InfoMangaComponent},
  {path: 'manga/:nameManga/:chapterName/:id', component: MangaChapterComponent},
  {path: 'manga/addManga', component: CreateMangaComponent},
  { path: 'account', redirectTo: '/sign-in', pathMatch: 'full' },
  { path: 'sign-in', component: SignInComponent },
  { path: 'register-user', component: SignUpComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'verify-email-address', component: VerifyEmailComponent }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
