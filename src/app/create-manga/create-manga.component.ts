import {Component, OnInit} from '@angular/core';
import {mangaService} from "../service/manga.service";
import {AngularFirestore, AngularFirestoreDocument} from "@angular/fire/compat/firestore";
import {Manga} from "../model/manga";
import firebase from "firebase/compat";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-create-manga',
  templateUrl: './create-manga.component.html',
  styleUrls: ['./create-manga.component.scss']
})
export class CreateMangaComponent implements OnInit {

  constructor(private mangaService: mangaService,
              private fire: AngularFirestore,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
  }

  mangaData: any;
  mangaName: any;
  searchName: any;
  idUniq: any;
  interval: any
  chapters: any;
  uploadFire: boolean = true;
  searchList: any;
  mangaListLoaded: boolean = false;
  startLoader: boolean = false;
  showMore: boolean = false;

  ngOnInit(): void {

  }

  onSubmit(url: any) {
    this.mangaService.createManga(url).subscribe((result: any) => {
      this.mangaData = result;
      this.idUniq = url;
    }, error => {
      console.log("A aprut o erurare")
    }, () => {
      console.log("Completed")
      this.saveToFirebase();
      // this.test();
    });
  }

  onSearch(searchText: any) {
    this.mangaListLoaded = true;
    if(searchText != null){
      this.mangaService.searchManga(searchText).subscribe((result: any) => {
        this.searchList = result;

        if(this.searchList.searches.length != 0){
          this.mangaListLoaded = false;
        }else{
          this.mangaListLoaded = true;
        }

      });
    }

  }

  loadManga(mangaUrl: any){

    let url = mangaUrl.split('/')[4];

    this.mangaService.createManga(url).subscribe((result: any) => {
      this.mangaData = result;
      this.idUniq = url;
    }, error => {
      console.log("There is an error!!")
    }, () => {
      console.log("Completed")
      this.saveToFirebase();

      // this.test();
    });
  }


  //Salvare/Primire data primita de la springboot/java - nu e pentru reinitializare

  saveToFirebase(): void {

    const mangaRef: AngularFirestoreDocument<Manga> = this.fire.doc('getManga/' + this.idUniq);

    this.mangaData.id = this.fire.createId();


    this.mangaData.chapters.forEach( (res:any) => {
      res.id = this.fire.createId();

      if(res.chapterImages != null){
        res.chapterImages.forEach((resultImage:any) => {
          resultImage.id = this.fire.createId();
        })
      }

    })
    mangaRef.set(this.mangaData, {
      merge: true

    }).then(r => {

      console.log("show nothing");
    })

    let mangaTitle = this.mangaData.mangaUrl.split('/')[4];
    this.router.navigate(["info-manga/" + mangaTitle + "/" + this.mangaData.id])
  }

  getDataChapter(){
   this.fire.doc("getManga/" + this.idUniq).valueChanges().subscribe(  s=> {
     this.chapters = s;
   },error => {
     console.log("Error for loading data");
   },() => {
   })
  }


  ngOnDestroy() {
    if (!this.uploadFire) {
      clearInterval(this.interval);
    }
  }

}
