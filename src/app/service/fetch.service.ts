import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class fetchService{
  private baseUrl = 'http://localhost:8081/api/';
  constructor(private http: HttpClient) { }

  createAccount(account: any){
      const reqHeader = new HttpHeaders({'Content-Type': 'application/json','No-Auth': 'True'});
      return this.http.post(this.baseUrl+ 'account',account,{headers: reqHeader});
  }




}


