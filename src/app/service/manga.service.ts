import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, switchMap} from "rxjs";

@Injectable({
  providedIn: 'root'
})

export  class mangaService{
  private baseUrl = 'http://192.168.178.72:8081/api/';

  constructor(private http: HttpClient) {
  }

  createManga(url: any): Observable<any> {
    const reqHeader = new HttpHeaders({'Content-Type': 'application/json', 'No-Auth': 'True'});
    return this.http.get(this.baseUrl+'manga/'+url, {headers: reqHeader});
  }

  createChapterImages(mangaName: any, ch: any): Observable<any> {
    const reqHeader = new HttpHeaders({'Content-Type': 'application/json', 'No-Auth': 'True'});
    return this.http.get(this.baseUrl+'earlyMangaImages/'+mangaName+ "/" + ch, {headers: reqHeader});
  }

  getManga(id: any): Observable<any> {
    const reqHeader = new HttpHeaders({'Content-Type': 'application/json', 'No-Auth': 'True'});
    return this.http.get(this.baseUrl+'/manga/info/'+id, {headers: reqHeader});
  }

  getChapter(id:any){
    const reqHeader = new HttpHeaders({'Content-Type': 'application/json', 'No-Auth': 'True'});
    return this.http.get(this.baseUrl+'/manga/chapter/'+id, {headers: reqHeader});
  }

  searchManga(searchText: any){
    const reqHeader = new HttpHeaders({'Content-Type': 'application/json', 'No-Auth': 'True'});
    return this.http.get(this.baseUrl+'searchManga/'+searchText, {headers: reqHeader});
  }

}
