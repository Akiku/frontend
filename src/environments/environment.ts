// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBqtVaCA1oCxsvtGTDHgtyxmMFGuB707IE",
    authDomain: "mangaakiku.firebaseapp.com",
    databaseURL: "https://mangaakiku-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "mangaakiku",
    storageBucket: "mangaakiku.appspot.com",
    messagingSenderId: "490959816104",
    appId: "1:490959816104:web:789538768ee539228f499d"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
